package time;
import static org.junit.Assert.*;
import org.junit.Test;
import time.Time;
/**
 * 
 * @author Luc Laffin
 *
 */
public class TimeTest {
	
	@Test
	public void testGetTotalMillisecondsRegular() {
		int totalMilliSeconds = Time.getTotalMilliSeconds("12:05:50:05");
		assertTrue("Invalid number of milliseconds", totalMilliSeconds == 5);
	}
	
	@Test(expected = NumberFormatException.class)
	public void testGetTotalMilliSecondsExceptional() {
		int totalMilliSeconds = Time.getTotalMilliSeconds("12:05:50:2A");
		fail("Invalid number of milliseconds");
	}
	
	@Test
	public void testGetTotalMilliSecondsBoundaryIn() {
		int totalMilliSeconds = Time.getTotalMilliSeconds("12:00:00:999");
		assertTrue("Invalid number of milliseconds", totalMilliSeconds == 999);
	}
	
	/**
	 * 
	 * @author Luc Laffin
	 *
	 */
	
	@Test(expected=NumberFormatException.class)
	public void testGetTotalMilliSecondsBoundaryOut() {
		int totalMilliSeconds = Time.getTotalMilliSeconds("12:05:50:1000");
		assertTrue("Invalid number of milliseconds", totalMilliSeconds != 1000);
	}
	
	
	
	///////////////////////////////
	
	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:04");
		assertTrue("The time provided does not match the result", totalSeconds == 3664);
	}
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsExceptional() {
		int totalSeconds = Time.getTotalSeconds("01:01:01C");
		fail("The time provided is not valid");
	}
	@Test
	public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds = Time.getTotalSeconds("00:00:59");
		assertTrue("The time provided does not match the result", totalSeconds == 59);
	}
	@Test (expected=AssertionError.class)
	public void testGetTotalSecondsBoundaryOut() {
		int totalSeconds = Time.getTotalSeconds("00:01:60");
		fail("The time provided is not valid");
	}
}
